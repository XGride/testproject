all: clean tests
	echo "all"

tests:
	virtualenv venv && . venv/bin/activate && pip install -r requirements.txt && pytest add_test.py && deactivate

clean:
	rm -rf venv
