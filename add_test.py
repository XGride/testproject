from add import add
import pytest

def test1():
	assert add(1,5) == 6

def test2():
	assert add(2,5) != 8

def test3():
	assert add(-400,567302) > 0
